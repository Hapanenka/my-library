package by.andrey.springcorse.ShopApp.repositories;


import by.andrey.springcorse.ShopApp.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {
}
