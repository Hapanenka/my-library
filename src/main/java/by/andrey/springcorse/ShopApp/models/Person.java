package by.andrey.springcorse.ShopApp.models;

import jdk.jfr.Timestamp;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "person")
public class Person {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 3, max = 30, message = "The name must be in the range of 3 to 30 characters")
    //  @NotEmpty(message = "Not Null")
    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "year")
    @Min(value = 1900, message = "You should specify the real year of birth")
    @Max(value = 2100, message = "You should specify the real year of birth")
    private int year;

    @Column(name = "registration")
    //   @Timestamp
    private LocalDateTime registration;

    @Column(name = "phone", length = 20)
    @Size(min = 3, max = 20, message = "Enter your Phone")
    private String phone;

    @Column(name = "password", length = 100)
    //  @Size(min = 3, max = 100, message = "Your pass must be in the range of 3 to 100 characters")
    //   @NotEmpty(message = "Not Null")
    private String password;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private PersonRole role;

    @OneToMany(mappedBy = "owner")
    private List<Book> book;

    public List<Book> getBook() {
        return book;
    }

    public void setBook(List<Book> book) {
        this.book = book;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public LocalDateTime getRegistration() {
        return registration;
    }

    public void setRegistration(LocalDateTime registration) {
        this.registration = registration;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PersonRole getRole() {
        return role;
    }

    public void setRole(PersonRole role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", phone='" + phone + '\'' +
                '}';
    }
}


