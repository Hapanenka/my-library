package by.andrey.springcorse.ShopApp.models;

public enum PersonRole {
    USER,
    MANAGER,
    ADMIN
}
