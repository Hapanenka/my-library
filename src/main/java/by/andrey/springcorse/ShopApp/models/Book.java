package by.andrey.springcorse.ShopApp.models;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Table(name = "book")
public class Book {

    @Id
    @Column(name = "book_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int book_id;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private BookType type;

    @Size(min = 3, max = 30, message = "The name must be in the range of 3 to 30 characters")
    @Column(name = "name", unique = true)
    private String name;

    @Size(min = 3, max = 30, message = "The author must be in the range of 3 to 30 characters")
    @Column(name = "author")
    private String author;

    @Column(name = "year")
    @Min(value = 0, message = "Specify the year of writing the book")
    @Max(value = 2100, message = "Specify the year of writing the book")
    private int year;

    @Column(name = "when_add")
    private LocalDateTime when_add;  // in base

    @Column(name = "who_add")
    private String who_add;

    @ManyToOne
    @JoinColumn(name = "person_id", referencedColumnName = "id")
    private Person owner;

    @Transient
    private boolean expired;

    public boolean isExpired() {
        return expired;
    }

    public Book() {
    }

    public Book(int book_id, BookType type, String name, String author, int year, LocalDateTime when_add,
                String who_add, Person owner, boolean expired) {
        this.book_id = book_id;
        this.type = type;
        this.name = name;
        this.author = author;
        this.year = year;
        this.when_add = when_add;
        this.who_add = who_add;
        this.owner = owner;
        this.expired = expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public BookType getType() {
        return type;
    }

    public void setType(BookType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public LocalDateTime getWhen_add() {
        return when_add;
    }

    public void setWhen_add(LocalDateTime when_add) {
        this.when_add = when_add;
    }

    public String getWho_add() {
        return who_add;
    }

    public void setWho_add(String who_add) {
        this.who_add = who_add;
    }


    public int getBook_id() {
        return book_id;
    }

    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

}