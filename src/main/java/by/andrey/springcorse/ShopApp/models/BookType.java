package by.andrey.springcorse.ShopApp.models;

public enum BookType {
    ADVENTURE,
    BIOGRAPHY,
    DICTIONARY,
    DRAMA,
    WESTERN,
    BUSINESS,
    HISTORY,
    PHILOSOPHY,
    SCIENCE,
    FANTASY,
    FICTION,
    CRIME,
    HORROR,
    MYSTERY,
    ROMANCE,
    SATIRE,
    THRILLER,
    SCIENCE_FICTION,
    FAIRYTALE

    //и много других вариантов, будет грустно добавлю
}
