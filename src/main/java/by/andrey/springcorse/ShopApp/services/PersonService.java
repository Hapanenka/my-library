package by.andrey.springcorse.ShopApp.services;

import by.andrey.springcorse.ShopApp.models.Person;
import by.andrey.springcorse.ShopApp.repositories.PeopleRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PersonService {

    private final PeopleRepository peopleRepository;


    public PersonService(PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    public boolean findByName(String name) {
       return peopleRepository.findByName(name).isPresent();
    }

    public List<Person> findAll() {
        return peopleRepository.findAll();
    }

    public List<Person> findWithPagination(Integer page, Integer PersonDTOPerPage, boolean sortByYear) {
        if (sortByYear)
            return peopleRepository.findAll(PageRequest.of(page, PersonDTOPerPage, Sort.by("year"))).getContent();
        else
            return peopleRepository.findAll(PageRequest.of(page, PersonDTOPerPage)).getContent();
    }

    public Object findName(String name) {
        return peopleRepository.findByName(name);
    }

    public Person findOne(int id){
        Optional<Person> foundPerson = peopleRepository.findById(id);
        return foundPerson.orElse(null);
    }

    @Transactional
    public void update(String name, Person updatedPerson){
        updatedPerson.setName(name);
        peopleRepository.save(updatedPerson);
    }
}