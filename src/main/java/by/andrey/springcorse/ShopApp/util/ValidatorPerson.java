package by.andrey.springcorse.ShopApp.util;

import by.andrey.springcorse.ShopApp.models.Person;
import by.andrey.springcorse.ShopApp.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ValidatorPerson implements Validator {

    private final PersonService personService;

    @Autowired
    public ValidatorPerson(PersonService personService) {
        this.personService = personService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Person.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Person person = (Person) o;
        if (personService.findByName(person.getName()))
            errors.rejectValue("name", "", "This name is already registered");
    }
}
