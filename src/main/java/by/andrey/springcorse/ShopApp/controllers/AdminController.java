package by.andrey.springcorse.ShopApp.controllers;

import by.andrey.springcorse.ShopApp.dto.PersonDtoService;
import by.andrey.springcorse.ShopApp.models.Book;
import by.andrey.springcorse.ShopApp.models.Person;
import by.andrey.springcorse.ShopApp.services.AdminService;
import by.andrey.springcorse.ShopApp.services.BookService;
import by.andrey.springcorse.ShopApp.services.PersonService;
import by.andrey.springcorse.ShopApp.util.ErrorResponse;
import by.andrey.springcorse.ShopApp.util.NotCreatedException;
import by.andrey.springcorse.ShopApp.util.PersonNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
@PreAuthorize("hasAuthority('ADMIN')")
public class  AdminController {

    private final AdminService adminService;

    private final PersonDtoService personDtoService;

    private final PersonService personService;
    private final BookService bookService;
    @Autowired
    public AdminController(AdminService adminService, PersonDtoService personDtoService, PersonService personService,
                           BookService bookService) {
        this.adminService = adminService;
        this.personDtoService = personDtoService;
        this.personService = personService;
        this.bookService = bookService;
    }

    @GetMapping("/people")
    public String index(Model model, @RequestParam(value = "page", required = false) Integer page,
                        @RequestParam(value = "personDAO_per_page", required = false) Integer personPerPage,
                        @RequestParam(value = "sort_by_year", required = false) boolean sortByYear) {
        if (page == null || personPerPage == null)
            model.addAttribute("person", personService.findAll());
        else
            model.addAttribute("person", personService.findWithPagination(page, personPerPage, sortByYear));
        return "admin/people";
    }

    @GetMapping("/books")
    public String indexBook(Model model, @RequestParam(value = "page", required = false) Integer page,
                        @RequestParam(value = "book_per_page", required = false) Integer bookPerPage,
                        @RequestParam(value = "sort_by_year", required = false) boolean sortByYear) {

        if (page == null || bookPerPage == null)
            model.addAttribute("book", bookService.findAll(sortByYear)); // выдача всех книг
        else
            model.addAttribute("book", bookService.findWithPagination(page, bookPerPage, sortByYear));

        return "admin/books";
    }
    @GetMapping("/book/{id}")
    public String showBook(@PathVariable("id") int id, Model model, @ModelAttribute("person") Person person) {
        model.addAttribute("book", bookService.findOne(id));
        Person bookOwner = bookService.getBookOwner(id);
        if (bookOwner != null)
            model.addAttribute("owner", bookOwner);
        else
            model.addAttribute("people", personService.findAll());

        return "/admin/showbook";
    }

    @GetMapping("/{id}")
    public String show(@PathVariable("id") int id, Model model) {
        model.addAttribute("person", adminService.findById(id));
        return "admin/show";
    }

    @GetMapping("/start")
    public String adminPage() {
        return "/admin/start";
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id){       //УДАЛЯЕТ ВСЕХ И СЕБЯ ТОЖЕ
        adminService.deleteById(id);
        return "redirect:/admin/people";
    }

    @PostMapping("/newBook")
    public String create(@ModelAttribute("book") @Valid Book book,
                         BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return "admin/newBook";
        bookService.save(book);
        return "redirect:/admin/books";
    }

    @GetMapping("/newBook")
    public String addNewBook() {
        return "/admin/newBook";
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotCreatedException e){
        ErrorResponse response = new ErrorResponse(e.getMessage(),
                System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(PersonNotFoundException e){
        ErrorResponse response = new ErrorResponse("Person with this id was`t found",
                System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
}