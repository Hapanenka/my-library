package by.andrey.springcorse.ShopApp.controllers;

import by.andrey.springcorse.ShopApp.services.PersonService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/user")
public class PersonController {

    private final PersonService personService;


    public PersonController(PersonService personService) {
        this.personService = personService;
    }


    @GetMapping("/start")
    public String adminPage() {
        return "/user/start";
    }

}