package by.andrey.springcorse.ShopApp.controllers;

import by.andrey.springcorse.ShopApp.models.Person;
import by.andrey.springcorse.ShopApp.security.RegistrationService;
import by.andrey.springcorse.ShopApp.services.PersonService;
import by.andrey.springcorse.ShopApp.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping
public class AuthController {

    private final RegistrationService registrationService;

    private final ValidatorPerson validatorPerson;

    private final PersonService personService;

    @Autowired
    public AuthController(RegistrationService registrationService,
                          ValidatorPerson validatorPerson, PersonService personService) {
        this.registrationService = registrationService;
        this.validatorPerson = validatorPerson;
        this.personService = personService;
    }

    @PostMapping("/registration")
    public String create(@ModelAttribute("person") @Valid Person person,
                         BindingResult bindingResult) {
        validatorPerson.validate(person, bindingResult);
        if (bindingResult.hasErrors())
            return "/registration";
        registrationService.register(person);
            return "redirect:/login";

    }

    @GetMapping("/registration")
    public String registrationPage(@ModelAttribute("person") Person person) {
        return "registration";
    }

    @GetMapping("/login")
    public String loginPage() {
        return "login";
    }

    @GetMapping
    public String helloBoot() {
        return "login";
    }

    @GetMapping("/hello")
    public String helloPage() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ADMIN"))) {
            return "/admin/start";
        }
        if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("MANAGER"))) {
            return "/manager/start";
        }
        else
            return "redirect:/user/start";
    }

    @PatchMapping("/updatePerson")
    public String updatePerson(@ModelAttribute("person") @Valid Person person,
                         BindingResult bindingResult) {
        validatorPerson.validate(person, bindingResult);
        /*Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

            String name = authentication.getName();*/
        if (bindingResult.hasErrors())
            return "/updatePerson";
        registrationService.register(person);
        return "redirect:/hello";

    }

    @GetMapping("/updatePerson")
    public String updatePage() {
        return "/updatePerson";
    }
}