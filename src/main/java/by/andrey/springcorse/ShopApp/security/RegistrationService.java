package by.andrey.springcorse.ShopApp.security;

import by.andrey.springcorse.ShopApp.models.Book;
import by.andrey.springcorse.ShopApp.models.Person;
import by.andrey.springcorse.ShopApp.models.PersonRole;
import by.andrey.springcorse.ShopApp.repositories.PeopleRepository;
import by.andrey.springcorse.ShopApp.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.LocalDateTime;

@Service
public class RegistrationService {
    private final PeopleRepository peopleRepository;
    private final PasswordEncoder passwordEncoder;
    private final BookRepository bookRepository;

    @Autowired
    public RegistrationService(PeopleRepository peopleRepository, PasswordEncoder passwordEncoder, BookRepository bookRepository) {
        this.peopleRepository = peopleRepository;
        this.passwordEncoder = passwordEncoder;
        this.bookRepository = bookRepository;
    }

    @Transactional
    public void register(Person person) {
        person.setPassword(passwordEncoder.encode(person.getPassword()));
        person.setRole(PersonRole.USER);
        person.setRegistration(LocalDateTime.now());
        peopleRepository.save(person);

    }

    public void addProduct(Book book) {
//        book.setCreationDate(LocalDateTime.now());
        bookRepository.save(book);
    }
}